const path=require('path');
const etp=require('extract-text-webpack-plugin');

const extract=new etp({
    filename: 'main.css'
});

module.exports = {
    entry:'./js/app.js',
    output : {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist'
    },
    module:{
        rules:[
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015']
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: extract.extract({
                    use: ['css-loader','sass-loader']
                })
            }
        ]
    },
    plugins:[
        extract
    ]
    
};